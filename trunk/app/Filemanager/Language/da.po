msgid ""
msgstr ""
"Project-Id-Version: ZooCMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-12-27 19:49+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: Jan <jkp@cusix.dk>\n"
"Language-Team: ZooCMS Crew\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Danish\n"
"X-Poedit-Country: DENMARK\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-Basepath: E:\\Dokumenter\\Xoops\\webserver\\ZooCMS SVN\\app\n"
"X-Poedit-KeywordsList: setLegend;_;Zend_Controller_Action_Exception;translate;setLabel;addMessage\n"
"X-Poedit-SearchPath-0: Filemanager\n"

#: Filemanager/Controllers/FileController.php:193
msgid "Order"
msgstr "Rækkefølge"

#: Filemanager/Controllers/FileController.php:201
msgid "Per page"
msgstr "Per side"

#: Filemanager/Controllers/FileController.php:245
msgid "Image removed"
msgstr "Billedet er fjernet"

#: Filemanager/Controllers/FileController.php:251
#, php-format
msgid "Are you sure, you want to remove %s from the gallery?"
msgstr "Er du sikker på, at du vil fjerne %s?"

#: Filemanager/Controllers/FileController.php:332
msgid "An error occurred during file upload, please try again"
msgstr "Der skete en fejl under upload, prøv venligst igen"

#: Filemanager/Controllers/FileController.php:355
msgid "Category does not exist"
msgstr "Kategorien eksisterer ikke"

#: Filemanager/Controllers/FileController.php:425
msgid "Content not found"
msgstr "Indhold ikke fundet"

#: Filemanager/Controllers/FileController.php:431
msgid "Access denied - insufficient privileges"
msgstr "Du har ikke adgang til dette indhold"

#: Filemanager/Hook/Node.php:72
msgid "Select file"
msgstr "Vælg fil"

#: Filemanager/Hook/Node.php:103
msgid "File upload"
msgstr "Upload fil"

#: Filemanager/Views/scripts/file/browse.phtml:3
#: Filemanager/Views/scripts/file/categories.ajax.phtml:2
msgid "Folders"
msgstr "Mapper"

#: Filemanager/Views/scripts/file/browse.phtml:13
msgid "Upload"
msgstr "Upload"

#: Filemanager/Views/scripts/file/browse.phtml:14
msgid "Refresh"
msgstr "Opdater"

#: Filemanager/Views/scripts/file/browse.phtml:15
msgid "Settings"
msgstr "Indstillinger"

#: Filemanager/Views/scripts/file/browse.phtml:17
msgid "Show Selected"
msgstr "Vis valgte"

#: Filemanager/Views/scripts/file/browse.phtml:18
msgid "Reorder"
msgstr "Omroker"

#: Filemanager/Views/scripts/file/browse.phtml:70
msgid "Please select a folder"
msgstr "Vælg en mappe"

#: Filemanager/Views/scripts/file/browse.phtml:181
#: Filemanager/Views/scripts/file/browse.phtml:187
#: Filemanager/Views/scripts/file/browse.phtml:193
msgid "Edit"
msgstr "Rediger"

#: Filemanager/Views/scripts/file/browse.phtml:183
msgid "Remove"
msgstr "Fjern"

#: Filemanager/Views/scripts/file/browse.phtml:189
msgid "Select"
msgstr "Vælg"

#: Filemanager/Views/scripts/file/browse.phtml:194
msgid "Delete"
msgstr "Slet"

#: Filemanager/Views/scripts/file/list.ajax.phtml:12
#: Filemanager/Views/scripts/file/sel-item.phtml:9
msgid "Image: "
msgstr "Billede:"

#: Filemanager/Views/scripts/file/list.ajax.phtml:16
msgid "File: "
msgstr "Fil:"

#: Filemanager/Views/scripts/hooks/list.phtml:2
msgid "Price"
msgstr ""

#: Filemanager/Views/scripts/hooks/list.phtml:3
msgid "Area"
msgstr ""

#: Filemanager/Views/scripts/hooks/list.phtml:4
msgid "Rooms"
msgstr ""

#: Filemanager/Views/scripts/hooks/list.phtml:5
msgid "Floors"
msgstr ""

#: Filemanager/Views/scripts/hooks/list.phtml:6
msgid "Built Year"
msgstr ""

#: Filemanager/Views/scripts/hooks/list.phtml:7
msgid "Ground area"
msgstr ""

#: Filemanager/Views/scripts/index/index.phtml:10
#: Filemanager/Views/scripts/node/filelist.phtml:6
#: Filemanager/Views/scripts/node/list.phtml:6
msgid "Image"
msgstr "Billede"

#: Filemanager/Views/scripts/index/index.phtml:17
#: Filemanager/Views/scripts/node/filelist.phtml:13
#: Filemanager/Views/scripts/node/list.phtml:13
msgid "File"
msgstr "Fil"

#: Filemanager/Views/scripts/node/index.phtml:16
#: Filemanager/Views/scripts/node/index.phtml:17
#: Filemanager/Views/scripts/node/index.phtml:18
msgid "Download"
msgstr "Download"

#~ msgid "Billede: "
#~ msgstr "Billede:"
#~ msgid "Billede"
#~ msgstr "Billede"

#, fuzzy
#~ msgid "Fil"
#~ msgstr "Fil"
#~ msgid "of"
#~ msgstr "af"
#~ msgid "Background colour"
#~ msgstr "Baggrundsfarve"
#~ msgid "Top image"
#~ msgstr "Topbillede"
#~ msgid "Background image"
#~ msgstr "Baggrundsbillede"
#~ msgid "Gallery extras"
#~ msgstr "Extra indstillinger"
#~ msgid "Images"
#~ msgstr "Billeder"
#~ msgid "Galleries"
#~ msgstr "Gallerier"
#~ msgid "%s images"
#~ msgstr "%s billeder"
#~ msgid "images"
#~ msgstr "Billeder"

